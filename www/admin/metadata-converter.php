 <?php

require_once('../_include.php');

$xmluri = '';

// make sure that the user has admin access rights
SimpleSAML\Utils\Auth::requireAdmin();

// Get global configuration
$config = SimpleSAML_Configuration::getInstance();

$baseDir = dirname(dirname(dirname(__FILE__)));
$metadataDir = $baseDir.'/metadata/';
$configDir = SimpleSAML\Utils\Config::getConfigDir().'/';

// Get metadata directory
        $sourcesConfig = $config->getArray('metadata.sources', null);

        // for backwards compatibility, and to provide a default configuration
        if ($sourcesConfig === null) {
            $type = $config->getString('metadata.handler', 'flatfile');
			$sourcesConfig = array(array('type' => $type));
        }

		foreach($sourcesConfig as $s) {
			if ($s['type']==='flatfile') {
				$metadataDir = $s['directory'];
			}
		}

// Get post data
if (array_key_exists('spName',$_POST)) {
	$spName = $_POST['spName'];
} else {
	$spName='';
}
if (array_key_exists('spType',$_POST)){
	$spType=$_POST['spType'];
} else {
	$spType="";
}

if (array_key_exists('xmluri', $_POST)&&$_POST['xmluri']!=='') {
    $xmluri = $_POST['xmluri'];
} elseif (!empty($_FILES['xmlfile']['tmp_name'])) {
    $xmldata = trim(file_get_contents($_FILES['xmlfile']['tmp_name']));
} elseif (array_key_exists('xmldata', $_POST)) {
    $xmldata = trim($_POST['xmldata']);
}

if ($xmluri!="") {
	$xmldata= trim(file_get_contents($xmluri));
}

if (!empty($xmldata)) {
    \SimpleSAML\Utils\XML::checkSAMLMessage($xmldata, 'saml-meta');
    $entities = SimpleSAML_Metadata_SAMLParser::parseDescriptorsString($xmldata);

    // get all metadata for the entities
    foreach ($entities as &$entity) {
        $entity = array(
            'shib13-sp-remote'  => $entity->getMetadata1xSP(),
            'shib13-idp-remote' => $entity->getMetadata1xIdP(),
            'saml20-sp-remote'  => $entity->getMetadata20SP(),
            'saml20-idp-remote' => $entity->getMetadata20IdP(),
        );
    }

    // transpose from $entities[entityid][type] to $output[type][entityid]
    $output = SimpleSAML\Utils\Arrays::transpose($entities);

    // merge all metadata of each type to a single string which should be added to the corresponding file
    foreach ($output as $type => &$entities) {
        $text = '';
        foreach ($entities as $entityId => $entityMetadata) {

            if ($entityMetadata === null) {
                continue;
            }

            // remove the entityDescriptor element because it is unused, and only makes the output harder to read
            unset($entityMetadata['entityDescriptor']);

            $content= '$metadata['.var_export($entityId, true).'] = '.var_export($entityMetadata, true).";\n";

			if (($spName!=="")&&($spType===$type)) {

// Create the provider output
			    $fn = $metadataDir . $type.'.php';
	            if ( @file_put_contents($fn, "<?php  \n" . $content) === false ) {
	                $text .= "Write failed for $fn\n\n";
    	        } else {
        	        $text .= "Written as $fn\n\n";
            	}

// Use a template to generate an entry into authsources.php
	            $fn = $configDir . "$type.tmpl";
	            $tas = @file_get_contents($fn);
				if ($tas === FALSE) {
					$text .= "Could not read template $fn\n\n";
				} else {
					$found = FALSE;
	            	$idp = var_export($entityId, true);
		            $idp_source = str_replace("{{idp}}",$idp,$tas);

					eval('$idp_tmp = '. "$idp_source;");
					ob_start();
						var_export($idp_tmp);
						$idp_source=ob_get_contents();
					ob_end_clean();

			        $authSources = SimpleSAML_Configuration::getConfig('authsources.php');
					$as = "<?php\n\n\$config = array (\n\n";

			        $sources = $authSources->getOptions();
			        foreach ($sources as $id) {
			            $source = $authSources->getArray($id);
		            	ob_start();
						var_export($source);
						$src = ob_get_contents();
						ob_end_clean();

						if ($id === $spName) {
							$src = $idp_source;
							$found = True;
						}
						$as .=  "'$id' => $src,\n\n" ;
					}

					if ($found===FALSE) {
						$as .=  "'$spName' => $idp_source,\n\n" ;
					}

					$as .=  ");\n\n";

		            $fn = $configDir.'authsources.php';

		            if ( @file_put_contents($fn,$as) === false ) {
		                $text .= "Failed to write $fn\r\r";
	    	        } else {
	        	        $text .= "Written as $fn\r\r";
	            	}
				}
			}
            $text .=  $content;
        }
        $entities = $text;
    }
} else {
    $xmldata = '';
    $output = array();
}

if ($xmluri!='') {
	$xmldata = '';
}

$template = new SimpleSAML_XHTML_Template($config, 'metadata-converter.php', 'admin');
$template->data['clipboard.js'] = true;
$template->data['xmldata'] = $xmldata;
$template->data['output'] = $output;
$template->data['xmluri'] = $xmluri;
$template->data['spName'] = $spName;
$template->data['spType'] = $spType;
$template->show();
